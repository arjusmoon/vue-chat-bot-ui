# Chat Bot UI - (Vue Js + Node Js)

A pure Vue + Node js Chatbot UI develped from scratch by [Social Beat](https://socialbeat.in/)

### Prerequisites!

- Node.js
- npm (optional for windows as Node comes with npm installed by default. But required when hosting the app on ubuntu)

### Installation

Server by default run on http://localhost:8080/:

```sh
$ git clone https://arjusmoon@bitbucket.org/arjusmoon/vue-chat-bot-ui.git
$ npm i
$ npm run serve
```

For production environments...

```sh
$ git clone https://arjusmoon@bitbucket.org/arjusmoon/vue-chat-bot-ui.git
$ npm i
$ npm run build
```
